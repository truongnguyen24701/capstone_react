import { https } from "./configURL"

export const movieSer = {
    getMovieList: () => {
        return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP04")
    },
    getMovieDetail: (id) => {
        return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`)
    },
    getMovieByTheater: () => {
        return https.get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP04")
    },
    getTicketMovie: () => {
        return https.get("/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=17881")
    }
}