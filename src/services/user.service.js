import { https } from "./configURL"

export const userSer = {
    postLogin: (loginData) => {
        return https.post("/api/QuanLyNguoiDung/DangNhap", loginData)
    },
    postResgiter: (resgiterData) => {
        return https.post("/api/QuanLyNguoiDung/DangKy", resgiterData)
    },
}