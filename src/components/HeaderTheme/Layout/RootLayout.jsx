import { Button, Dropdown, Layout, Menu } from 'antd';
import React from 'react';
import HeaderTheme from '../HeaderTheme';
import UserNav from '../UserNav';
import { NavLink } from 'react-router-dom';
import { useMediaQuery } from 'react-responsive';
import { PoweroffOutlined } from '@ant-design/icons';

const { Header, Content } = Layout;

const menu = (
    <Menu
        items={[
            {
                key: '1',
                label: (
                    <a href="/">
                        Trang chủ
                    </a>
                ),
            },
            {
                key: '2',
                label: (
                    <a >
                        Liên hệ
                    </a>
                ),
            },
            {
                key: '3',
                label: (
                    <a >
                        Tin tức
                    </a>
                ),
            },
            {
                key: '4',
                label: (
                    <a >
                        Ứng dụng
                    </a>
                ),
            },
        ]}
    />
);

const RootLayout = ({ children }) => {
    const Desktop = useMediaQuery({ minWidth: 751 })
    return (
        <Layout className="layout"  >
            <Header className='flex justify-between items-center' >
                <HeaderTheme />
                <div id='top' className='rouded px-5 py-2 text-white'>
                    {
                        Desktop ? <>
                            <NavLink to={"/"}>
                                <button>Trang chủ</button>
                            </NavLink>
                            <button className='mx-5'>Liên hệ</button>
                            <button>Tin tức</button>
                            <button className='mx-5'>Ứng dụng</button>
                        </> : <Dropdown overlay={menu} placement="bottomLeft" arrow>
                            <Button className='border-blue-500 text-blue-500 rouded px-5'>Mục lục</Button>
                        </Dropdown>
                    }
                </div>
                <UserNav />
            </Header>
            <Content
               
            >
                {children}
            </Content>

        </Layout>
    )
};

export default RootLayout