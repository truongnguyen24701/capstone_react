import React from 'react'
import { NavLink } from 'react-router-dom';

export default function HeaderTheme() {
    return (
        <NavLink to={"/"}>
            <img style={{ width: "100px", height: "45px", objectFit: "cover", borderRadius: "10px" }} src="https://ephoto360.com/uploads/worigin/2019/06/19/Tao_Logo_Team_Online_5d09d219c1294_940c172c0e84c1f53f0c57d4bf684291.jpg" alt="" />
        </NavLink>
    )
}
