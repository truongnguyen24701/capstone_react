import React from 'react'
import { useState } from 'react'
import { useEffect } from 'react'
import { bannerSer } from '../../../services/banner.Service'
import { SwiperSlide, Swiper } from 'swiper/react';
import { Navigation, Pagination } from 'swiper';
// import Swiper and modules styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import './style.css'

export default function BannerHome() {
    const [banner, setBanner] = useState([])
    useEffect(() => {
        bannerSer
            .getBannerList()
            .then((res) => {
                console.log(res);
                setBanner(res.data.content)
            })
            .catch((err) => {
                console.log(err);
            })
    }, [])
    console.log("banner", banner);
    return (

        <>
            <Swiper
                slidesPerView={1}
                spaceBetween={30}
                loop={true}
                pagination={{
                    clickable: true,
                }}
                modules={[Pagination, Navigation]}
                className="mySwiper"
            >
                {
                    banner.map((item, key) => {
                        return (
                            <SwiperSlide>
                                <div style={{ width: "100%" }}>
                                    <img style={{ width: "100%", height: "650px", objectFit: "cover", objectPosition: "center" }} src={item.hinhAnh} alt="" />
                                </div>
                            </SwiperSlide>

                        )
                    })
                }

            </Swiper>
        </>
    )
}
