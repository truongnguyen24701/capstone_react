import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { localStorageServ } from './../../services/LocalStorageServ';
import { loginAction } from './../../Pages/redux/action/userAction';
import { useNavigate } from 'react-router-dom';
import { UserOutlined } from '@ant-design/icons';
import { Avatar, Dropdown, Menu } from 'antd';

export default function UserNav() {
  const history = useNavigate()
  let dispatch = useDispatch()
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor
  })
  

  const handleLogout = () => {
    localStorageServ.user.remove()
    dispatch(loginAction(null))
    history('/login')
  }

  const menu = (
    <Menu
      items={userInfor ? [{
        key: '1',
        label: (
          <a >
            {userInfor.hoTen}
          </a>
        ),
      },
      {
        key: '2',
        label: (
          <a
            onClick={handleLogout}
            href="/login">
            Đăng xuất
          </a>
        ),
      },] : [
        {
          key: '1',
          label: (
            <a onClick={() => {
              window.location.href = "/login"
            }}>
              Đăng nhập
            </a>
          ),
        },
        {
          key: '2',
          label: (
            <a href="/resgiter">
              Đăng ký
            </a>
          ),
        },
      ]}
    />
  );
  return (
    <div>
      <Dropdown overlay={menu} placement="bottomLeft" arrow>
        <Avatar style={{width:"40px",height:"40px"}} shape="square" size={35} icon={<UserOutlined />} />
      </Dropdown>

    </div>


  )
}
