import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePages from './Pages/HomePages/HomePages'
import LoginPages from './Pages/LoginPages/LoginPages'
import DetailPages from './Pages/DetailPages/DetailPages';
import Resgiter from './Pages/Resgiter/Resgiter';
import NotFoundPage from './Pages/NotFoundPage/NotFoundPage';
import AdminPage from './Pages/AdminPage/AdminPage';
import TicketPage from './Pages/TicketPage/TicketPage';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path='/login' element={<LoginPages />} />
          <Route path='/resgiter' element={<Resgiter />} />
          <Route path='/' element={<HomePages />} />
          <Route path='detail/:id' element={<DetailPages />} />
          <Route path='*' element={<NotFoundPage />} />
          <Route path='/admin' element={<AdminPage />} />
          <Route path='/ticket' element={<TicketPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
