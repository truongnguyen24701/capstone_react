import React from 'react'
import './style.css'
import { UpCircleOutlined } from '@ant-design/icons';

export default function FooterPage() {
    return (
        <div className='footer-movie font-medium'>
            <div className='footer-list'>
                <div className='footer-right'>
                    <img src="https://ephoto360.com/uploads/worigin/2019/06/19/Tao_Logo_Team_Online_5d09d219c1294_940c172c0e84c1f53f0c57d4bf684291.jpg" alt="" />
                    <p>197 Học Viện Công Nghệ Bưu Chính Viễn Thông
                        Quận 9, Thành Phố Hồ Chí Minh</p>
                </div>
                <div className='footer-left'>
                    <div className='left-1'>
                        <div className='left-chinh-sach'>
                            Chính sách
                        </div>
                        <button>Terms of Use</button>
                        <button>Privacy Policy</button>
                        <button>Security</button>
                    </div>
                    <div>
                        <div className='left-2'>
                            <div className='left-tai-khoan'>
                                Tài Khoản
                            </div>
                            <button>My Account</button>
                            <button>Watchlist</button>
                            <button>Collections</button>
                            <button>User Guide</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className='footer-end'>
                <p>Nguyễn Xuân Trường & Hồng Quyến</p>
                <a href="#top">Back to top <UpCircleOutlined /></a>
            </div>
        </div>
    )
}







