import React from 'react'
import BannerHome from '../../components/HeaderTheme/BannerHome/BannerHome'
import RootLayout from '../../components/HeaderTheme/Layout/RootLayout'
import FooterPage from '../FooterPage/FooterPage'
import ListMovie from './ListMovie'
import TabMovie from './TabMovie'
import { HashLoader } from 'react-spinners';
import { useSelector, useDispatch } from 'react-redux';
import { useState } from 'react'
import { useEffect } from 'react'
import { movieSer } from './../../services/movie.Service';


export default function HomePages() {
    const [dataMovie, setDataMovie] = useState([])
    const { isLoading } = useSelector(state => state.loadingReducer)
    console.log("isloading", isLoading);

    useEffect(() => {
        movieSer
            .getMovieByTheater()
            .then((res) => {
                console.log(res);
                setDataMovie(res.data.content)
            })
            .catch((err) => {
                console.log(err);
            })
    }, [])

    return (
        <>
            {
                isLoading ? <div style={{ width: "100%", height: "100vh", alignItems: "center", display: "flex", justifyContent: "center" }}>
                    <HashLoader style={{ width: "100px", height: "100px" }} isLoading={false} color="#36d7b7" />
                    <ListMovie />

                </div> : <>
                    <RootLayout>
                        <div className=''>
                            <BannerHome />
                            <ListMovie />
                            <TabMovie  dataMovie={dataMovie} />
                        </div>
                    </RootLayout>
                    <FooterPage /></>
            }


        </>
    )
}
