import { Card } from 'antd';
import Meta from 'antd/lib/card/Meta';
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { getMovieListActionSev } from './../redux/action/movieAction';
import { NavLink } from 'react-router-dom';
import { useMediaQuery } from 'react-responsive';
import { tatLoadingAction } from '../redux/action/loadingAction';
import { batLoadingAction } from './../redux/action/loadingAction';


export default function ListMovie() {
    let dispatch = useDispatch();
    const Desktop = useMediaQuery({ minWidth: 992 })
    const Tablet = useMediaQuery({ minWidth: 768, maxWidth: 991 })
    const Mobile = useMediaQuery({ minWidth: 500, maxWidth: 767 })
    const Default = useMediaQuery({ minWidth: 0, maxWidth: 499 })
    useEffect(() => {
        console.log(Desktop, Tablet, Mobile)
    }, [Desktop, Tablet, Mobile])
    let { movieList } = useSelector((state) => {
        return state.movieReducer;
    })
    console.log("movieList", movieList);

    useEffect(() => {
        dispatch(getMovieListActionSev());
    }, [])

    useEffect(() => {
        if (movieList.length > 0) {
            dispatch(tatLoadingAction())
        } else {
            dispatch(batLoadingAction())
        }
    }, [movieList])
    let renderMovieList = () => {
        return movieList
            .map((item, key) => {
                return (
                    <Card

                        key={key}
                        hoverable
                        style={{
                            width: "100%",
                            display: "flex",
                            flexDirection: "column",
                        }}
                        bodyStyle={{
                            height: '100%',
                            display: 'flex',
                            flexDirection: 'column'
                        }}

                        className="shadow-xl hover:shadow-slate-800 hover:shadow-xl "
                        cover={
                            <img
                                style={{ width: "100%", height: "350px", borderRadius: "7px", objectFit: "cover" }}
                                alt=""
                                src={item.hinhAnh} />
                        }
                    >
                        <Meta className='truncate' style={{
                            flex: 1
                        }} title={<p className='text-blue-500' >{item.tenPhim}</p>}
                            description={item.moTa}
                        />

                        <NavLink to={`detail/${item.maPhim}`}>
                            <button className='w-full bg-red-600 text-white rounded text-xl font-medium py-3 my-3'>Mua vé</button>
                        </NavLink>
                    </Card>
                )
            })


    }
    return <div className={`grid ${Desktop ? "grid-cols-3" : Tablet ? "grid-cols-2" : Mobile ? "grid-cols-1" : Default ? "grid-cols-1" : "grid-cols-1"}  gap-10 mx-20 my-5`}>{renderMovieList()}</div>

}
