import { Tabs } from 'antd'
import TabPane from 'antd/lib/tabs/TabPane'
import React from 'react'
import { useEffect, useState } from 'react'
import { movieSer } from '../../services/movie.Service'
import ItemTabMovie from './ItemTabMovie'
import { useDispatch } from 'react-redux';
import { tatLoadingAction } from '../redux/action/loadingAction'
import { batLoadingAction } from './../redux/action/loadingAction';

export default function TabMovie({dataMovie}) {
    const onChange = (key) => {
        console.log(key);
    }

    const renderContent = () => {

        return dataMovie && dataMovie.map((heThongRap, index) => {
            return (
                <TabPane tab={<img className='w-20' src={heThongRap.logo} />}
                    key={index}
                >
                    <Tabs style={{ height: 500 }} tabPosition='left' defaultActiveKey="1" onChange={onChange}>
                        {heThongRap.lstCumRap.map((cumRap, index) => {
                            return (
                                <TabPane tab={renderTenCumRap(cumRap)} key={cumRap.maCumRap} >
                                    <div style={{ height: 500, overflow: "auto" }} >
                                        {cumRap.danhSachPhim.map((phim) => {
                                            return <ItemTabMovie phim={phim} />
                                        })}
                                    </div>
                                </TabPane>
                            )
                        })}

                    </Tabs>
                </TabPane>
            )
        })
    }

    const renderTenCumRap = (cumRap) => {
        return <div className='text-left w-60 truncate'>
            <p className='text-green-700'>{cumRap.tenCumRap}</p>
            <p className='truncate'>{cumRap.diaChi}</p>
            <button className='text-red-600'>[ Xem chi tiết ]</button>
        </div>
    }

    return (
        <div className='ml-20 py-20'>
            <Tabs
                style={{ height: 500 }}
                tabPosition='left' defaultActiveKey="1" onChange={onChange}>
                {renderContent()}
            </Tabs>
        </div>
    )
}
