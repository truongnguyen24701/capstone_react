import React, { useEffect } from 'react'
import RootLayout from '../../components/HeaderTheme/Layout/RootLayout'
import { useDispatch, useSelector } from 'react-redux';
import { getTicketActionSevr } from './../redux/action/ticketAction';
import { Card, Col, Row } from 'antd';
import FooterPage from '../FooterPage/FooterPage';
import { batLoadingAction, tatLoadingAction } from './../redux/action/loadingAction';
import { HashLoader } from 'react-spinners';

export default function TicketPage() {
    const { isLoading } = useSelector(state => state.loadingReducer)
    console.log("isLoading", isLoading);
    let dispatch = useDispatch()
    let { danhSachGhe, thongTinPhim } = useSelector((state) => {
        return state.ticketReducer
    })
    useEffect(() => {
        dispatch(getTicketActionSevr())
        
    }, [])

    useEffect(() => {
        if (danhSachGhe.length > 0) {
            dispatch(tatLoadingAction())
        } else {
            dispatch(batLoadingAction())
        }
    }, [danhSachGhe])


    let RenderTicketMovie = () => {
        return (
            <div className='ml-40 mt-5'>
                <Row gutter={[10, 6]} style={{ width: "450px" }}>
                    {
                        danhSachGhe.map((item, key) => {
                            return (
                                <Col span={2} key={key}>
                                    {
                                        item.daDat == true ?
                                            <div className='rounded px-4 py-4 bg-green-600' ></div> :
                                            item.loaiGhe == "Thuong" ?
                                                <div className='rounded px-4 py-4 bg-gray-300'></div> :
                                                <div className='rounded px-4 py-4 bg-yellow-400'></div>
                                    }
                                </Col>
                            )
                        })
                    }
                </Row>
                <div className='flex my-5 ml-20'>
                    <div className='rounded bg-gray-300 px-2 py-2'></div>
                    <div className='mr-3'>Ghế thường</div>
                    <div className='rounded bg-yellow-400 px-2 py-2'></div>
                    <div className='mr-3'>Ghế VIP</div>
                    <div className='rounded bg-green-600 px-2 py-2'></div>
                    <div>Ghế đã đặt</div>

                </div>
            </div>
        )
    }

    console.log("thongTinPhim", thongTinPhim);

    let RenderCard = () => {
        return (
            <div className="site-card-border-less-wrapper font-medium mr-40">
                <Card
                    title={thongTinPhim.tenPhim}
                    bordered={false}
                    style={{
                        width: 470,
                    }}
                >
                    <div className='flex justify-between '>
                        <p>Ngày chiếu giờ chiếu</p>
                        <span>{thongTinPhim.ngayChieu} - {thongTinPhim.gioChieu}</span>

                    </div>
                    <hr />
                    <div className='flex justify-between '>
                        <p>Cụm rạp</p>
                        <span>{thongTinPhim.tenCumRap}</span>
                    </div>
                    <hr />
                    <div className='flex justify-between '>
                        <p>Rạp</p>
                        <span>{thongTinPhim.tenRap}</span>
                    </div>
                    <hr />
                    <button style={{ backgroundColor: "#D07000", paddingInline: "200px", color: "white" }}>Đặt vé</button>


                </Card>
            </div>
        )
    }

    return (
        <>
            {
                isLoading ? <div style={{ width: "100%", height: "100vh", alignItems: "center", display: "flex", justifyContent: "center" }}>
                    <HashLoader style={{ width: "100px", height: "100px" }} isLoading={false} color="#36d7b7" />
                </div> : <>
                    <div>
                        <RootLayout />
                        <div className='w-1/2 mt-2'>
                            <div className='bg-yellow-200 w-3/4 h-2 m-auto '></div>
                            <p className=' w-3/4 h-20 m-auto' style={{
                                clipPath: 'polygon(4% 0, 95% 0, 100% 100%, 0% 100%)',
                                background: "linear-gradient( gray, white)"
                            }}>Màn hình</p>
                        </div>
                        <div className='flex justify-between'>
                            <RenderTicketMovie />
                            <RenderCard />
                        </div>
                        <FooterPage />
                    </div>
                </>
            }
        </>

    )
}
