
import { movieSer } from './../../../services/movie.Service';
import { SET_TICKET } from './../constants/ticketConstants';


export const getTicketActionSevr = () => {
    return (dispatch) => {
        movieSer
            .getTicketMovie()
            .then((res) => {
                dispatch({
                    type: SET_TICKET,
                    payload: res.data.content
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }
}