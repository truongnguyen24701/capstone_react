import { movieSer } from "../../../services/movie.Service"
import { SET_MOVIE_LIST } from "../constants/movieConstants";


export const getMovieListActionSev = () => {
    return (dispatch) => {
        movieSer
            .getMovieList()
            .then((res) => {
                console.log(res);
                dispatch({
                    type: SET_MOVIE_LIST, 
                    payload: res.data.content
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }
}