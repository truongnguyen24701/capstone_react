import { combineReducers } from "@reduxjs/toolkit";
import { userReducer } from './userReducer';
import { movieReducer } from './movieReducer';
import { ticketReducer } from "./ticketReducer";
import { loadingReducer } from './loadingReducer';

export const rootReducer = combineReducers({
    userReducer,
    movieReducer,
    ticketReducer,
    loadingReducer,
})