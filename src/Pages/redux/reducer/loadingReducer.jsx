
import { BAT_LOADING, TAT_LOADING } from './../constants/loadingContants';

const initialState = {
    isLoading: true
}

export const loadingReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case BAT_LOADING: {
            return { ...state, isLoading: true }
        }
        case TAT_LOADING: {
            return { ...state, isLoading: false }
        }
        default:
            return state
    }
}