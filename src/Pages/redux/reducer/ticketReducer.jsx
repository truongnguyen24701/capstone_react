
let initalState = {
    thongTinPhim: [],
    danhSachGhe: []
}

export const ticketReducer = (state = initalState, action) => {
    switch (action.type) {
        case "SET_TICKET": {
            state.thongTinPhim = action.payload.thongTinPhim;
            state.danhSachGhe = action.payload.danhSachGhe;
            return { ...state }
        }
        default:
            return state
    }
}