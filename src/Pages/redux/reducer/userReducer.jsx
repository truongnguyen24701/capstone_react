import { localStorageServ } from "../../../services/LocalStorageServ";


let initalState = {
    userInfor: localStorageServ.user.get()
}

export const userReducer = (state = initalState, action) => {
    switch (action.type) {
        case "LOGIN": {
            state.userInfor = action.payload;
            return { ...state }
        }
        default:
            return state
    }
}