import { Form, Input, message } from 'antd';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate, Route } from 'react-router-dom';
import { userSer } from './../../services/user.service';
import { loginAction } from './../redux/action/userAction';
import { localStorageServ } from './../../services/LocalStorageServ';
import LoginAnimate from './LoginAnimate';

export default function LoginPages() {
    let dispatch = useDispatch()
    let history = useNavigate()
    useEffect(() => {

        const data = localStorage.getItem('USER')
        if (data) {
            if (JSON.parse(localStorage.getItem('USER')).maLoaiNguoiDung == "QuanTri") {
                history("/admin")
            } else {
                history("/")
            }
        }

    }, [])
    const onFinish = (values) => {
        console.log('Success:', values);
        userSer
            .postLogin(values)
            .then((res) => {
                message.success("Đăng nhập thành công")
                dispatch(loginAction(res.data.content))
                localStorageServ.user.set(res.data.content)

                setTimeout(() => {
                    if (JSON.parse(localStorage.getItem('USER')).maLoaiNguoiDung == "QuanTri") {
                        history("/admin")
                    } else {
                        history("/")
                    }
                }, 1000)
            })
            .catch((err) => {
                message.error(err?.response?.data.content)
            })
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div className='bg-green-300  p-10' style={{ height: "100vh" }}>
            <div className='container m-auto bg-white p-10 rounded-xl flex'>
                <div style={{ height: '600px', width: '600px' }}>
                    <LoginAnimate />
                </div>
                <div className='w-1/2 pt-60'>
                    <Form className='pr-20'
                        name="basic"
                        labelCol={{
                            span: 8,
                        }}
                        wrapperCol={{
                            span: 16,
                        }}
                        initialValues={{
                            remember: true,
                        }}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                    >
                        <Form.Item
                            label={<p className='font-medium text-blue-700'>Tài khoản</p>}
                            name="taiKhoan"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập tài khoản!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label={<p className='font-medium text-blue-700'>Mật khẩu</p>}
                            name="matKhau"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập mật khẩu!',
                                },
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>
                        <div className='ml-20'>
                            <button className='rounded px-5 py-2 text-white bg-blue-500 ml-40'>Đăng nhập</button>
                        </div>
                        <div className='ml-20'>
                            <button
                                onClick={() => {
                                    window.location.href = "/resgiter"
                                }}
                                className='rounded px-5 py-2 text-white bg-green-500 ml-40 mt-5'>Tạo tài khoản mới</button>
                        </div>
                    </Form>
                </div>
            </div>
        </div>
    );
};