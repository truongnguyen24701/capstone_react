import React from 'react'
import { movieSer } from './../../services/movie.Service';
import { useParams } from 'react-router';
import { useState, useEffect } from 'react';
import { Progress } from 'antd';
import RootLayout from '../../components/HeaderTheme/Layout/RootLayout';
import TabMovie from '../HomePages/TabMovie';
import FooterPage from '../FooterPage/FooterPage';
import ReactStars from 'react-stars';
import { useSelector, useDispatch } from 'react-redux';
import { HashLoader } from 'react-spinners';
import { tatLoadingAction } from '../redux/action/loadingAction';
import { batLoadingAction } from './../redux/action/loadingAction';

export default function DetailPages() {
    const [dataMovie, setDataMovie] = useState([])
    const dispatch = useDispatch()
    const { id } = useParams()
    const [movie, setMovie] = useState({})
    const { isLoading } = useSelector(state => state.loadingReducer)
    console.log("isLoading", isLoading);

    useEffect(() => {
        movieSer
            .getMovieDetail(id)
            .then((res) => {
                setMovie(res.data.content)
                console.log(res);

            })
            .catch((err) => {
                console.log(err);
            })

    }, [])

    useEffect(() => {
        movieSer
            .getMovieByTheater()
            .then((res) => {
                console.log(res);
                setDataMovie(res.data.content)
            })
            .catch((err) => {
                console.log(err);
            })
    }, [])


    useEffect(() => {
        if (dataMovie.length > 0) {
            dispatch(tatLoadingAction())
        } else {
            dispatch(batLoadingAction())
        }
    }, [dataMovie])

    return (
        <>
            {
                isLoading ? <div style={{ overflow: "hidden", height: "100vh" }}> <div style={{ width: "100%", height: "100vh", alignItems: "center", display: "flex", justifyContent: "center" }}>
                    <HashLoader style={{ width: "100px", height: "100px" }} isLoading={false} color="#36d7b7" />
                </div>
                    <TabMovie />
                </div> : <>
                    <RootLayout>
                        <div className='container mx-auto bg-blue-100' >
                            <div className='flex px-20 py-10 space-x-10'>
                                <img src={movie.hinhAnh} className="w-80" alt="" />
                                <div
                                    style={{ width: "100%", height: "350px", }}
                                    className='text-left'>
                                    <p className='text-5xl font-medium text-red-500 pb-5'>{movie.tenPhim}</p>
                                    <hr />
                                    <p className=' font-medium'>Mô tả: {movie.moTa}</p>
                                    <button className=' px-5 py-2 text-white bg-red-500 mr-10 rouded font-medium mt-10'>
                                        <a href={movie.trailer} target="_blank">Xem Trailer</a>
                                    </button>
                                    <button
                                        onClick={() => {
                                            window.location.href = "/ticket"
                                        }}
                                        className='px-5 py-2 text-white bg-red-500 mr-10 rouded font-medium mt-10'>
                                        Mua vé ngay</button>
                                </div>
                                <div>
                                    <Progress type='circle' percent={movie.danhGia * 10} format={(number) => {
                                        return (
                                            <span className='text-black font-medium'>{number / 10}</span>
                                        )
                                    }} />
                                    <ReactStars
                                        count={5}
                                        onChange={movie.danhGia}
                                        size={24}
                                        color2={'#ffd700'} />
                                </div>
                            </div>
                        </div>
                        <div className='mx-20'>
                            <TabMovie dataMovie={dataMovie} />
                        </div>
                        <FooterPage />
                    </RootLayout></>
            }
        </>

    )
}
