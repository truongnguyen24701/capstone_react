import React from 'react'
import { Form, Input, message } from 'antd';
import { userSer } from './../../services/user.service';
import { useDispatch } from 'react-redux';
import { resgiterAction } from '../redux/action/userAction';
import { useNavigate } from 'react-router-dom';
import LoginAnimate from './../LoginPages/LoginAnimate';


export default function Resgiter() {
    let dispatch = useDispatch()
    let history = useNavigate()
    const onFinish = (values) => {
        console.log("Success", values);
        userSer
            .postResgiter(values)
            .then((res) => {
                message.success("Đăng ký thành công")
                dispatch(resgiterAction(res.data.content))

                setTimeout(() => {
                    history("/")
                }, 1000)
            })
            .catch((err) => {
                message.error(err?.response?.data.content)
            })
    }

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (

        <div className='bg-green-300  p-10'>
            <div className=' bg-white p-10 rounded-xl flex'>
                <div style={{ height: '600px', width: '600px' }}>
                    <LoginAnimate />
                </div>
                <div className='w-1/2 mt-20'>
                    <Form
                        name="basic"
                        labelCol={{
                            span: 8,
                        }}
                        wrapperCol={{
                            span: 16,
                        }}
                        initialValues={{
                            remember: true,
                        }}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                    >

                        <Form.Item
                            label={<p className='font-medium text-blue-700'>Tài khoản</p>}
                            name="taiKhoan"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập tài khoản!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label={<p className='font-medium text-blue-700'>Mật khẩu</p>}
                            name="matKhau"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập mật khẩu!',
                                },
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item
                            label={<p className='font-medium text-blue-700'>Nhập lại mật khẩu</p>}
                            name="nhapLaiMatKhau"
                            rules={[
                                {
                                    required: true,
                                    message: 'Mật khẩu không đúng!',
                                },
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item
                            label={<p className='font-medium text-blue-700'>Họ và tên</p>}
                            name="hoTen"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập họ và tên!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label={<p className='font-medium text-blue-700'>Email</p>}
                            name="email"
                            rules={[
                                {
                                    required: false,
                                    pattern: new RegExp(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
                                    message: 'Vui lòng nhập đúng địa chỉ email!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label={<p className='font-medium text-blue-700'>Số điện thoại</p>}
                            name="soDT"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập số điện thoại!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                        <div className='ml-20  mb-5'>
                            <button className='rounded ml-40 px-5 py-2 text-white bg-red-500 mr-10'>Đăng ký</button>
                        </div>
                        <div className='pl-60'>
                            Nếu bạn đã có tài khoản. Trở về <a className='font-medium text-blue-700' href="/">Trang chủ</a> để đăng nhập
                        </div>

                    </Form>
                </div>
            </div>
        </div>
    )
}
